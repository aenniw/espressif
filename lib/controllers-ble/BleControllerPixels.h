#pragma once

#include <commons.h>
#include <Pixels.h>
#include <BleServer.h>
#include <PixelsRepository.h>

class BleControllerPixels : public Subscriber<BleServer> {
public:
    const static NimBLEUUID UUID_COLOR, UUID_BRIGHTNESS, UUID_PARAMS,
            UUID_COLORS, UUID_STATE, UUID_LENGTH, UUID_ENABLED;
private:
    Pixels &pixels;
    PixelsRepository &repository;
    const NimBLEUUID UUID;

    BLECharacteristic *colorCharacteristic = nullptr;
    BLECharacteristic *stateCharacteristic = nullptr;
    BLECharacteristic *modeCharacteristic = nullptr;
    BLECharacteristic *brightnessCharacteristic = nullptr;
protected:
    void enabled(BLECharacteristic &c) const;
    void length(BLECharacteristic &c) const;
    bool set_length(BLECharacteristic &c);
    void state(BLECharacteristic &c, pixel::state s) const;
    bool set_state(BLECharacteristic &c);
    void color(BLECharacteristic &c, pixel::color color) const;
    bool set_color(BLECharacteristic &c);
    void brightness(BLECharacteristic &c, uint8_t b) const;
    bool set_brightness(BLECharacteristic &c);
    void params(BLECharacteristic &c, pixel::params p) const;
    bool set_params(BLECharacteristic &c);
    void colors(BLECharacteristic &c) const;
    bool set_colors(BLECharacteristic &c);
public:
    explicit BleControllerPixels(Pixels &pixels, PixelsRepository &repository,
                                 uint8_t i = 0, uint32_t uuid = 0xab5ff770) :
            pixels(pixels), repository(repository), UUID(fullUUID(uuid + i)) {}

    void toggle_state();
    void toggle_modes();
    void cycle_brightness(bool notify);
    void cycle_color(bool notify);
    void cycle_speed(bool notify);
    bool disabled() const;

    void subscribe(BleServer &ble) override;
};
