#include "BleControllerPixels.h"

bool BleControllerPixels::disabled() const {
    return pixels.get_length() == 0u;
}

void BleControllerPixels::enabled(BLECharacteristic &c) const {
    c.setValue(!disabled());
}

void BleControllerPixels::length(BLECharacteristic &c) const {
    auto l = repository.get_length();
    log_i("ble - length %d", l);
    c.setValue(l);
}

bool BleControllerPixels::set_length(BLECharacteristic &c) {
    repository.set_length(c.getValue<uint8_t>());
    return true;
}

void BleControllerPixels::state(BLECharacteristic &c, pixel::state s) const {
    log_i("ble - state %d", s);
    c.setValue(s);
}

bool BleControllerPixels::set_state(BLECharacteristic &c) {
    const auto s = (pixel::state) c.getValue<uint8_t>();
    pixels.set_state(s);
    repository.set_state(s);
    return true;
}

void BleControllerPixels::color(BLECharacteristic &c, pixel::color color) const {
    log_i("ble - color %d %d", color.hue, color.sat);
    c.setValue(color);
}

bool BleControllerPixels::set_color(BLECharacteristic &c) {
    const pixel::color color = c.getValue<pixel::color>();
    pixels.set_color(color);
    repository.set_color(color);
    return true;
}

void BleControllerPixels::brightness(BLECharacteristic &c, uint8_t b) const {
    log_i("ble - brightness %d", b);
    c.setValue(b);
}

bool BleControllerPixels::set_brightness(BLECharacteristic &c) {
    auto b = c.getValue<uint8_t>();
    pixels.set_brightness(b);
    repository.set_brightness(b);
    return true;
}

void BleControllerPixels::params(BLECharacteristic &c, pixel::params p) const {
    log_i("ble - mode %d %d %d %d %d", p.mode, p.duration, p.randomized, p.chained, p.partition);
    c.setValue(p);
}

bool BleControllerPixels::set_params(BLECharacteristic &c) {
    const auto p = c.getValue<pixel::params>();
    pixels.set_params(p);
    repository.set_params(p);
    return true;
}

void BleControllerPixels::colors(BLECharacteristic &c) const {
    repository.get_colors([&](uint8_t l, pixel::color colors[]) {
        log_i("ble - colors %d", l);
        c.setValue(reinterpret_cast<uint8_t *>(colors), l * sizeof(pixel::color));
    });
}

bool BleControllerPixels::set_colors(BLECharacteristic &c) {
    auto msg = c.getValue();
    auto len = msg.length() / sizeof(pixel::color);
    auto colors = reinterpret_cast<pixel::color *>((uint8_t *) msg.data());
    pixels.set_colors(len, colors);
    repository.set_colors(len, colors);
    return true;
}

const NimBLEUUID  BleControllerPixels::UUID_COLOR = fullUUID(0x05f3704eu);
const NimBLEUUID  BleControllerPixels::UUID_BRIGHTNESS = fullUUID(0x604d979du);
const NimBLEUUID  BleControllerPixels::UUID_PARAMS = fullUUID(0xa7601c29u);
const NimBLEUUID  BleControllerPixels::UUID_COLORS = fullUUID(0xb532fb4eu);
const NimBLEUUID  BleControllerPixels::UUID_STATE = fullUUID(0xb533fb4eu);
const NimBLEUUID  BleControllerPixels::UUID_LENGTH = fullUUID(0xb535fb4eu);
const NimBLEUUID  BleControllerPixels::UUID_ENABLED = fullUUID(0xb537fb4eu);

void BleControllerPixels::subscribe(BleServer &ble) {
    repository.configure(pixels);

    ble.on(UUID, UUID_ENABLED, std::bind(&BleControllerPixels::enabled, this, std::placeholders::_1));
    ble.on(UUID, UUID_LENGTH,
           std::bind(&BleControllerPixels::length, this, std::placeholders::_1),
           std::bind(&BleControllerPixels::set_length, this, std::placeholders::_1));
    stateCharacteristic = ble.on(
            UUID, UUID_STATE,
            [&](BLECharacteristic &c) { state(c, pixels.get_state()); },
            std::bind(&BleControllerPixels::set_state, this, std::placeholders::_1)
    );
    colorCharacteristic = ble.on(
            UUID, UUID_COLOR,
            [&](BLECharacteristic &c) { color(c, pixels.get_color()); },
            std::bind(&BleControllerPixels::set_color, this, std::placeholders::_1));
    brightnessCharacteristic = ble.on(
            UUID, UUID_BRIGHTNESS,
            [&](BLECharacteristic &c) { brightness(c, pixels.get_brightness()); },
            std::bind(&BleControllerPixels::set_brightness, this, std::placeholders::_1)
    );
    modeCharacteristic = ble.on(
            UUID, UUID_PARAMS,
            [&](BLECharacteristic &c) { params(c, pixels.get_params()); },
            std::bind(&BleControllerPixels::set_params, this, std::placeholders::_1)
    );
    ble.on(UUID, UUID_COLORS,
           std::bind(&BleControllerPixels::colors, this, std::placeholders::_1),
           std::bind(&BleControllerPixels::set_colors, this, std::placeholders::_1));
}

void BleControllerPixels::toggle_state() {
    state(*stateCharacteristic, pixels.get_state() == pixel::ON ?
                                pixel::OFF : pixel::ON);
    set_state(*stateCharacteristic);
    stateCharacteristic->notify();
}

void BleControllerPixels::cycle_brightness(bool notify) {
    if (pixels.get_state() == pixel::OFF) {
        return;
    }

    static int inc = 5;
    const auto value = pixels.get_brightness();
    if (notify) {
        pixels.set_animate(true);
        brightness(*brightnessCharacteristic, value);
        set_brightness(*brightnessCharacteristic);
        brightnessCharacteristic->notify();
    } else {
        inc = value == 255 || value <= 10 ? -inc : inc;
        pixels.set_animate(false);
        pixels.set_brightness(min(max(value + inc, 10), 255));
    }
}

void BleControllerPixels::cycle_color(bool notify) {
    if (pixels.get_state() == pixel::OFF ||
        pixels.get_mode() != pixel::STATIC) {
        return;
    }

    static int sat_inc = 10, hue_inc = 8;
    auto value = pixels.get_color();
    if (notify) {
        color(*colorCharacteristic, value);
        set_color(*colorCharacteristic);
        colorCharacteristic->notify();
    } else {
        if (value.hue % 360 == 0) {
            sat_inc = value.sat % 100 == 0 ? -sat_inc : sat_inc;
            value.sat = min(max(value.sat + sat_inc, 0), 100);
        }
        hue_inc = (value.hue % 360 == 0 ? -hue_inc : hue_inc);
        value.hue = min(max(
                (int) (hue_inc * 2 * ((100 - value.sat) / 100.f)) + value.hue + hue_inc, 0
        ), 360);
        pixels.set_color(value);
    }
}

void BleControllerPixels::cycle_speed(bool notify) {
    if (pixels.get_state() == pixel::OFF ||
        pixels.get_mode() == pixel::STATIC) {
        return;
    }

    static int inc = 25;
    auto value = pixels.get_params();
    if (notify) {
        params(*modeCharacteristic, value);
        set_params(*modeCharacteristic);
        modeCharacteristic->notify();
    } else {
        inc = value.duration % 1800u == 0 ? -inc : inc;
        value.duration = min(max((int) value.duration + inc, 0), 1800);
        pixels.set_params(value);
    }
}

void BleControllerPixels::toggle_modes() {
    if (pixels.get_state() == pixel::OFF) {
        return;
    }

    pixel::mode m = pixels.get_mode();
    pixel::params p = pixels.get_params();

    if (m == pixel::STATIC) {
        p.mode = (pixel::mode) ((m + 1) % (pixel::TRANSITION + 1));
    } else if (p.randomized && p.chained) {
        do {
            m = (pixel::mode) ((m + 1) % (pixel::TRANSITION + 1));
        } while (m == pixel::TRANSITION && pixels.get_colors_size() == 0);

        p.mode = m;
        p.randomized = false;
        p.chained = false;
    } else if (!p.chained) {
        p.chained = true;
    } else if (!p.randomized) {
        p.chained = false;
        p.randomized = true;
    }

    params(*modeCharacteristic, p);
    set_params(*modeCharacteristic);
    modeCharacteristic->notify();
}