#include "Pixels.h"

const static auto BLACK = RgbColor(0, 0, 0);

HsbColor pixel::to_color(const pixel::color c) {
    return {c.hue / 360.0f, c.sat / 100.0f, 1};
}

pixel::state Pixels::get_state() const {
    return state;
}

void Pixels::set_state(const pixel::state s) {
    log_i("pixels - set_state - %d", s);
    state = s;
}

pixel::color Pixels::get_color() const {
    return color;
}

void Pixels::set_color(pixel::color c) {
    log_i("pixels - set_color - %d %d - %d", c.hue, c.sat, refresh);
    color = c;
}

uint16_t Pixels::get_length() const {
    return length;
}

pixel::mode Pixels::get_mode() const {
    return (pixel::mode) params.mode;
}

pixel::params Pixels::get_params() const {
    return params;
}

void Pixels::set_animate(const bool animate) {
    if (animate && animator.IsPaused()) {
        animator.Resume();
    } else if (!animate && animator.IsAnimating()) {
        animator.Pause();
    }
}

void Pixels::set_params(const pixel::params p) {
    log_i("pixels - set_params - %d %d %d %d %d", p.mode, p.duration, p.chained, p.randomized, p.partition);

    this->params = p;

    const auto mode = get_mode();
    if (mode == pixel::STATIC) {
        log_d("pixels - set_params - set_color");
        animator.StopAll();
    } else if (animator.IsAnimationActive(mode)) {
        log_d("pixels - set_params - scale_animation");
        animator.ChangeAnimationDuration(mode, 200u + params.duration);
    } else {
        log_d("pixels - set_params - set_animation");
        animator.StopAll();
        animator.StartAnimation(mode, 200u + params.duration, animations[mode]);
    }
}

void Pixels::set_colors(uint8_t l, pixel::color colors[]) {
    log_i("pixels - set_colors - %d", l);
    animation_colors.clear();
    for (uint8_t i = 0; i < l; i++) {
        log_d("pixels - set_colors - %d %d", colors[i].hue, colors[i].sat);
        animation_colors.push_back(colors[i]);
    }
}

uint8_t Pixels::get_colors_size() const { return this->animation_colors.size(); }

Pixels::Pixels(uint16_t len) : length(len), animator(len, NEO_CENTISECONDS),
                               animation({0xffffffffffffu, len, 0, 0.f}) {
    animations[pixel::mode::FADE] = std::bind(&Pixels::fade, this, std::placeholders::_1);
    animations[pixel::mode::RAINBOW] = std::bind(&Pixels::rainbow, this, std::placeholders::_1);
    animations[pixel::mode::TRANSITION] = std::bind(&Pixels::transition, this, std::placeholders::_1);
}

void Pixels::cycle() {
    if (state == pixel::state::OFF || get_length() == 0u) {
        set_pixels(BLACK, false);
    } else if (get_mode() == pixel::mode::STATIC) {
        set_pixels(to_color(color), true);
    } else if (animator.IsAnimating() && !animator.IsPaused()) {
        animator.UpdateAnimations();
    }
    show();
}

void Pixels::refresh(const AnimationParam &param) {
    animation.progress = param.progress;

    if (param.state == AnimationState_Started) {
        const uint16_t len = get_length() / (params.partition + 1u);

        animation.end = params.randomized ? random(len / 4, len) : len;
        animation.start = params.randomized ? random(len - animation.end) : 0;
        animation.end = animation.start + animation.end;
        animation.mask = params.randomized ?
                         (((uint64_t) get_random()) << 32u) + get_random() :
                         0xffffffffffffu;
        log_d("pixels - anim opts - %d~%d %d", animation.start, animation.end, len);
    } else if (param.state == AnimationState_Completed) {
        animator.RestartAnimation(param.index);
    }
}

static float progress_offset(const uint16_t l, const uint16_t i, float p) {
    p = p + ((float) i / (float) l);
    return p >= 1.0f ? p - 1.0f : p;
}

void Pixels::animate(
        const AnimationParam &param,
        const pixel::ColorSupplier &c,
        const pixel::ColorSupplier &s,
        const bool dim
) {
    refresh(param);

    const uint16_t len = get_length();
    const uint8_t partition = params.partition + 1u;
    const uint16_t segment_len = (len / partition) + (len % partition != 0 ? 1u : 0u);
    for (uint16_t i = 0; i < segment_len; i++) {
        boolean color_correct = false;
        HsbColor next_color = BLACK;

        if ((animation.mask >> (i % 64)) & 0x1u && !params.chained) {
            next_color = s(i, animation.progress);
            color_correct = true;
        } else if (animation.start <= i && i < animation.end && params.chained) {
            next_color = c(i, progress_offset(
                    animation.end - animation.start,
                    i - animation.start,
                    animation.progress
            ));
            color_correct = true;
        } else if (dim) {
            next_color = get_pixel(i).Dim(254);
        }

        for (uint8_t o = 0; o < partition; o++) {
            const uint16_t pos = (partition * i) + o;
            if (pos >= len) {
                break;
            }
            set_pixel(pos, next_color, color_correct);
        }
    }
}

static RgbColor linearBlend(const RgbColor &l, const RgbColor &m, const RgbColor &r, float progress) {
    progress = progress * 2;
    if (progress <= 1.0f) {
        return RgbColor::LinearBlend(l, m, progress);
    }
    return RgbColor::LinearBlend(m, r, progress - 1.0f);
}

void Pixels::fade(const AnimationParam &param) {
    const auto rgbColor = RgbColor(to_color(color));

    const pixel::ColorSupplier cs = [&](uint16_t i, float p) {
        return linearBlend(BLACK, rgbColor, BLACK, p);
    };
    this->animate(param, cs, cs, false);
}

void Pixels::rainbow(const AnimationParam &param) {
    const pixel::ColorSupplier cs = [&](uint16_t i, float p) -> HsbColor {
        return {p, 1, 1};
    };

    this->animate(param, cs, cs, true);
}

static HsbColor color_offset(const std::vector<pixel::color> &colors, const float p, const uint16_t i = 0) {
    if (colors.empty()) {
        return BLACK;
    }
    const auto color = colors[
            ((uint16_t) std::floor(colors.size() * p) + i) % colors.size()
    ];
    return to_color(color);
}

void Pixels::transition(const AnimationParam &param) {
    this->animate(param, [&](uint16_t i, float p) {
        return color_offset(animation_colors, p, i);
    }, [&](uint16_t i, float p) {
        return color_offset(animation_colors, p);
    }, false);
}

uint8_t pixel::power_scale(uint16_t mA, uint8_t V, uint16_t len) {
    const double powerApprox = 60.0 * V * len;
    const double powerCap = V * mA;

    if (powerApprox > powerCap) {
        return (uint8_t) (255 * (powerCap / powerApprox));
    } else {
        return 255;
    }
}