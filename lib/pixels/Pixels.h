#pragma once

#include <vector>
#include <commons.h>
#include <NeoPixelBrightnessBus.h>
#include <NeoPixelAnimator.h>

namespace pixel {
    typedef enum {
        STATIC,
        FADE,
        RAINBOW,
        TRANSITION
    } mode;

    typedef enum {
        OFF, ON
    } state;

    typedef struct {
        uint16_t hue: 9;
        uint16_t sat: 7;
    } color;

    typedef struct {
        uint32_t mode: 8;
        uint32_t duration: 16;
        uint32_t chained: 1;
        uint32_t randomized: 1;
        uint32_t partition: 4;
        uint32_t : 2;
    } params;

    typedef struct {
        uint64_t mask;
        uint16_t end;
        uint16_t start;
        float progress;
    } animation;

    typedef std::function<HsbColor(uint16_t i, float p)> ColorSupplier;

    uint8_t power_scale(uint16_t mA, uint8_t V, uint16_t len);

    HsbColor to_color(pixel::color c);
}

class Pixels : public Service {
protected:
    pixel::color color = {0u, 0u};
    pixel::state state = pixel::OFF;
    pixel::params params = {
            .mode=pixel::STATIC,
            .duration=0u,
            .chained=false,
            .randomized=false,
            .partition=0u
    };
    uint16_t length;
    NeoPixelAnimator animator;
    pixel::animation animation;
    std::vector<pixel::color> animation_colors;
    AnimUpdateCallback animations[pixel::mode::TRANSITION + 1];

    virtual void set_pixels(const HsbColor &c, bool correct) = 0;
    virtual RgbColor get_pixel(uint16_t i) = 0;
    virtual void set_pixel(uint16_t i, const HsbColor &c, bool correct) = 0;
    virtual void show() = 0;

    void animate(const AnimationParam &param,
                 const pixel::ColorSupplier &c,
                 const pixel::ColorSupplier &s,
                 bool dim);
    void refresh(const AnimationParam &param);
    void fade(const AnimationParam &param);
    void rainbow(const AnimationParam &param);
    void transition(const AnimationParam &param);
public:
    explicit Pixels(uint16_t len);
    void cycle() override;

    uint16_t get_length() const;

    pixel::state get_state() const;
    void set_state(pixel::state s);

    void set_color(pixel::color c);
    pixel::color get_color() const;

    virtual void set_brightness(uint8_t b) = 0;
    virtual uint8_t get_brightness() const = 0;

    pixel::params get_params() const;
    void set_params(pixel::params p);

    pixel::mode get_mode() const;
    void set_animate(bool animate);

    void set_colors(uint8_t l, pixel::color colors[]);
    uint8_t get_colors_size() const;
};


template<typename T_COLOR_FEATURE, typename T_METHOD, typename T_GAMMA>
class NeoPixels : private NeoPixelBrightnessBus<T_COLOR_FEATURE, T_METHOD>, public Pixels {
private:
    NeoGamma<T_GAMMA> gamma;
    double power_cap = 1.0;
    uint8_t brightness = 0u;
protected:
    void set_pixels(const HsbColor &c, const bool correct) override {
        if (correct) {
            this->ClearTo(gamma.Correct(RgbColor(c)));
        } else {
            this->ClearTo(c);
        }
    }
    void set_pixel(uint16_t i, const HsbColor &c, const bool correct) override {
        if (correct) {
            this->SetPixelColor(i, gamma.Correct(RgbColor(c)));
        } else {
            this->SetPixelColor(i, c);
        }
    }
    RgbColor get_pixel(uint16_t i) override { return this->GetPixelColor(i); }
    void show() override {
        if (this->CanShow()) {
            this->Show();
        }
    }
    void begin() override { this->Begin(); }
public:
    NeoPixels(uint16_t len, uint8_t pin, uint8_t power = 255u) :
            NeoPixelBrightnessBus<T_COLOR_FEATURE, T_METHOD>(len, pin), Pixels(len), power_cap(power / 255.0) {
        log_i("pixels - power_cap - %f", power_cap);
    }

    void set_brightness(uint8_t b) override {
        log_i("pixels - set_brightness - %d", b);
        NeoPixelBrightnessBus<T_COLOR_FEATURE, T_METHOD>::_brightness =
                (brightness = b) * power_cap;

        const auto mode = get_mode();
        if (mode == pixel::STATIC) {
            this->set_pixels(pixel::to_color(get_color()), true);
        } else if (animator.IsPaused()) {
            const AnimationParam p = {
                    animation.progress, mode, AnimationState_Progress
            };
            Pixels::animations[mode](p);
        }
    };

    uint8_t get_brightness() const override { return brightness; };
};

typedef std::function<Pixels *(uint8_t len, uint8_t power)> PixelsSupplier;

#define NeoPixelsRtm(gpio, channel) [](uint8_t len, uint8_t power) { \
    return new NeoPixels<NeoGrbFeature, NeoEsp32Rmt ## channel ## 800KbpsMethod, NeoGammaTableMethod>( \
        len, gpio, power \
    ); \
}
