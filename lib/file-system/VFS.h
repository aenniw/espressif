#pragma once

#if defined(ARDUINO_ARCH_ESP32)
#include <esp_idf_version.h>

#if ESP_IDF_VERSION > ESP_IDF_VERSION_VAL(3, 3, 5)
#include <LittleFS.h>
#define VFS LittleFS
#define VFS_LITTLE_FS
#else
#include <SPIFFS.h>
#define VFS SPIFFS
#define VFS_SPIFFS
#endif
#elif defined(ARDUINO_ARCH_ESP8266)
#include <LittleFS.h>
#define VFS LittleFS
#define VFS_LITTLE_FS
#endif