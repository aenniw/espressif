# ESPRESSIF based projects

[![Build Status](https://gitlab.com/aenniw/espressif/badges/main/pipeline.svg)](https://gitlab.com/aenniw/espressif/-/pipelines)

<table>
  <tr>
    <td><img src="docs/img/wemos-d1.png"></td>
    <td><img src="docs/img/wroom-d1.png"></td>
    <td><img src="docs/img/ttgo-t-display.jpg"></td>
  </tr> 
</table>

## Setup environment

```bash
pip install -r requirements.txt
```

## Build

```bash
./make
```