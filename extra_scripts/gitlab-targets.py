from distutils.version import LooseVersion
import gitlab
import os

Import("env")

env_name = env['PIOENV']
build_dir = env.subst("$BUILD_DIR")
progname = env.subst("$PROGNAME")


def detools_artifacts(*args, **kwargs):
    package_name = build_dir.split('/.pio/')[0].split('/')[-1]

    config = env.GetProjectConfig()
    if not config.get("detools", "src_file", default=None) is None:
        return
    project_id = config.get("gitlab", "project_id", default=os.getenv('CI_PROJECT_ID'))
    if not project_id:
        return

    gl = gitlab.Gitlab(
        private_token=config.get("gitlab", "private_token", default=os.getenv('CI_PRIVATE_TOKEN')),
        oauth_token=config.get("gitlab", "oauth_token", default=os.getenv('CI_OAUTH_TOKEN')),
        job_token=config.get("gitlab", "job_token", default=os.getenv('CI_JOB_TOKEN'))
    )

    project = gl.projects.get(project_id)
    packages = sorted(
        [
            p for p in project.packages.list()
            if p.name == package_name
        ],
        key=lambda p: LooseVersion(p.version),
        reverse=True
    )

    for package in packages:
        for file in package.package_files.list():
            if not env_name == file.file_name.split('.')[0]:
                continue

            src_file = '%s/%s-legacy.bin' % (build_dir, progname)
            with open(src_file, "wb") as firmware:
                firmware.write(
                    project.generic_packages.download(
                        package_name=package.name,
                        package_version=package.version,
                        file_name=file.file_name,
                    )
                )
            print('GL Fetched %s [%s] for deltools' % (package.name, src_file))
            return


if not env.GetProjectOption("build_type", default='release') == 'debug':
    env.AddPreAction("buildprog", detools_artifacts)

env.AddCustomTarget(
    "gl-detools", None, detools_artifacts, title="Fetch latest Gitlab release"
)
