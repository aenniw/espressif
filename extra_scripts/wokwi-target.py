from simple_websocket_server import WebSocketServer, WebSocket
import time
import base64
import json
import sys
import os

Import("env")

build_dir = env.subst("$BUILD_DIR")
progname = env.subst("$PROGNAME")

ELF_PATH = '%s/%s.elf' % (build_dir, progname)
HEX_PATH = '%s/%s.hex' % (build_dir, progname)
BIN_PATH = '%s/%s.bin' % (build_dir, progname)


class WokwiServer(WebSocket):
    def handle(self):
        msg = json.loads(self.data)
        if msg["type"] == "uartData":
            sys.stdout.buffer.write(bytearray(msg["bytes"]))
            sys.stdout.flush()
        elif msg["type"] == "aloha":
            if os.path.isfile(HEX_PATH):
                with open(HEX_PATH, 'r') as file:
                    hex_content = file.read()
            else:
                hex_content = base64_file(BIN_PATH)

            self.send_message(json.dumps({
                "type": "start",
                "elf": base64_file(ELF_PATH),
                "hex": hex_content,
            }))
        else:
            print("Unknown message", msg)


def base64_file(path: str):
    with open(path, 'rb') as file:
        return base64.b64encode(file.read()).decode('ascii')


def wokwi_launch(*args, **kwargs):
    project_id = env.GetProjectOption("wokwi_id")
    port = env.GetProjectOption("wokwi_port", default=9012)

    if project_id:
        server = WebSocketServer('localhost', port, WokwiServer)
        print("Web socket listening,", end=" ")
        print("Open https://wokwi.com/_alpha/wembed/%s?partner=platformio&port=%d" % (project_id, port))

        last_activity = time.time()
        while True:
            server.handle_request()
            if len(server.connections) > 0:
                last_activity = time.time()
            elif time.time() - last_activity > 20:
                print("Closing due to inactivity.")
                break
    else:
        print("Wokwi project id not defined.")


env.AddCustomTarget(
    "wokwi", ['buildprog'], wokwi_launch, title="Launch binary in wokwi project"
)
