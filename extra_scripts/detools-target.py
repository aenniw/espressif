from detools import create_patch_filenames
import os

Import("env")

build_dir = env.subst("$BUILD_DIR")
progname = env.subst("$PROGNAME")


def detools_patch(*args, **kwargs):
    config = env.GetProjectConfig()
    src_file = config.get(
        "detools", "src_file", default=('%s/%s-legacy.bin' % (build_dir, progname))
    )
    dst_file = config.get(
        "detools", "dst_file", default=('%s/delta.bin' % build_dir)
    )

    if os.path.isfile(src_file):
        create_patch_filenames(
            src_file, '%s/%s.bin' % (build_dir, progname), dst_file,
            compression='heatshrink'
        )
        print("Build detool patch [%s]." % dst_file)
    else:
        print("Binary [%s] not found for detools patch." % src_file)


if not env.GetProjectOption("build_type", default='release') == 'debug':
    env.AddPostAction("buildprog", detools_patch)

env.AddCustomTarget(
    "detools", ['buildprog'], detools_patch, title="Build detools Image"
)
