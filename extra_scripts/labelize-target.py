import esptool
import os
import qrcode
from PIL import Image, ImageFont, ImageDraw, ImageOps
from urllib.request import urlopen
import time


def img_path(path, size=None, crop=None):
    img = Image.open(open(
        '%s/%s' % (os.path.abspath(os.getcwd()), path), 'rb'
    ))
    img = img.convert('RGBA').convert('L')
    img = img.point(lambda p: 0 if p < 200 else 255)
    if crop:
        img = img.crop(
            (crop[0], crop[1], img.width - crop[0], img.height - crop[1])
        )
    if size:
        img = img.resize(size)
    return img


def img_qr(url, size):
    qr = qrcode.make(url).convert('RGB')
    qr = qr.resize((min(size), min(size)))
    return qr


def roboto(size):
    return ImageFont.truetype(
        urlopen(
            'https://github.com/opensourcedesign/fonts/blob/master/Roboto_v2.0/Roboto-Bold.ttf?raw=true'),
        size=size
    )


def label(url, name, model, ssid, pin, wifi=False, ble=False, invert=True):
    img = Image.new("RGB", (1392, 542), color=(255, 255, 255))

    qr = img_qr(url, img.size)
    img.paste(
        qr, (img.width - qr.width, int((img.height - qr.height) / 2))
    )

    def img_paste(i, size, offset=(0, 0), crop=None):
        img.paste(
            img_path(i, size, crop),
            (img.width - qr.width - size[0] -
             offset[0], img.height - size[1] - offset[1])
        )

    img_paste('../docs/logos/e-waste.jpg', (220, 220), (-50, 30))
    img_paste('../docs/logos/esp.png', (160, 160), (0, 250))
    if ble:
        img_paste('../docs/logos/ble.jpg', (160, 160), (140, 40))
    if wifi:
        img_paste('../docs/logos/wifi.png', (130, 130), (155, 40))

    fonts = {x: roboto(x) for x in [44, 56]}
    d = ImageDraw.Draw(img)

    def text_paste(x, y, text, font):
        d.text((x, y), text, fill=(0, 0, 0), font=fonts[font])

    text_paste(40, 40, name, 56)
    text_paste(320, 52, "MODEL: %s" % (model), 44)
    d.line([(40, 110), (img.width - qr.width, 110)], fill=(0, 0, 0), width=4)

    text_paste(40, 120, "SSID: %s" % (ssid), 44)
    text_paste(40, 170, "PIN: %s" % (pin), 44)
    text_paste(40, 250, "INPUT: DC 5V 1500mA", 44)
    text_paste(40, 300, "OUTPUT: DC 5V 1000mA", 44)
    text_paste(40, 460, "Made in Czechia", 44)

    if invert:
        img = ImageOps.invert(img)

    dir = 'labels'
    if not os.path.isdir(dir):
        os.mkdir(dir)

    img.save("%s/%s-label.png" % (dir, time.time()))


Import("env")


def labelize(*args, **kwargs):
    config = env.GetProjectConfig()
    ssid = config.get("label", "ssid", default=None)
    name = config.get("label", "name", default=ssid)
    url = config.get("label", "url", default=None)
    if ssid is None or name is None or url is None:
        return

    esp = esptool.get_default_connected_device(
        esptool.get_port_list(),
        port=None,
        connect_attempts=3,
        initial_baud=esptool.ESPLoader.ESP_ROM_BAUD
    )

    if not esp:
        return

    pin = 0
    uuid = 0
    efuse_mac = int.from_bytes(bytes(esp.read_mac()), byteorder='little')
    for i in [0, 8, 16]:
        pin |= ((efuse_mac >> (40 - i)) & 0xff) << i
        uuid |= ((efuse_mac >> (40 - (i + 24))) & 0xff) << i

    pin %= 1000000
    while pin / 100000 <= 0:
        pin *= 10

    label(
        model=env['PIOENV'],
        name=name.upper(),
        ssid='%s-%s' % (ssid, hex(uuid)[2:].upper().rjust(6, '0')),
        pin=pin % 1000000,
        url=url,
        ble=config.get("label", "ble", default=None),
        wifi=config.get("label", "wifi", default=None),
    )
    esp.hard_reset()


if not env.GetProjectOption("build_type", default='release') == 'debug':
    env.AddPostAction("upload", labelize)

env.AddCustomTarget(
    "labelize", None, labelize, title="Generate device label."
)
