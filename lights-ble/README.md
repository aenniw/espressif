# BLE - Light controller

![lights-ble](../docs/img/lights-ble.png)

## Models

#### wroom32uc3ch

- STLs: [cap.stl](models/wroom32uc3ch/cap.stl), [base.stl](models/wroom32uc3ch/base.stl), [button.stl](models/wroom32uc3ch/button.stl), [insets.stl](models/insets.stl), [shade.stl](models/shade.stl)
- GERBERs: [wroom32uc3ch.zip](gerbers/wroom32uc3ch.zip)

#### mhetesp32minikit

- STLs: [cap.stl](models/mhetesp32minikit/cap.stl), [base.stl](models/mhetesp32minikit/base.stl), [button.stl](models/mhetesp32minikit/button.stl), [insets.stl](models/insets.stl), [shade.stl](models/shade.stl)
- GERBERs: [mhetesp32minikit.zip](gerbers/mhetesp32minikit.zip)