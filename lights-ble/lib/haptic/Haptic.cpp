#include "Haptic.h"

void Haptic::begin() {
    leftButton.setClickTicks(300);
    leftButton.setPressTicks(400);
    leftButton.attachClick([](void *param) { ((Haptic *) param)->cycle_chains(); }, this);
    leftButton.attachDoubleClick([](void *param) { ((Haptic *) param)->toggle_state(); }, this);
    leftButton.attachDuringLongPress([](void *param) { ((Haptic *) param)->cycle_brightness(); }, this);
    leftButton.attachLongPressStop([](void *param) { ((Haptic *) param)->save_brightness(); }, this);

    rightButton.setClickTicks(300);
    rightButton.setPressTicks(400);
    rightButton.attachClick([](void *param) { ((Haptic *) param)->toggle_modes(); }, this);
    rightButton.attachDuringLongPress([](void *param) { ((Haptic *) param)->cycle_options(); }, this);
    rightButton.attachLongPressStop([](void *param) { ((Haptic *) param)->save_options(); }, this);

    for (auto indicator : indicators) {
        pinMode(indicator, OUTPUT);
    }

    selected = controllers.size() - 1;
    elapsedInactivity = 0;
    cycle_chains();
}

void Haptic::cycle() {
    leftButton.tick();
    rightButton.tick();

    if (!inactive && elapsedInactivity >= 60) {
        log_i("haptics - toggle indicators - off");
        digitalWrite(indicators[selected], LOW);
        inactive = true;
    }
}

bool Haptic::allDisabled() {
    bool disabled = controllers.empty();
    for (auto &controller:controllers)
        disabled &= controller->disabled();
    return disabled;
}

bool Haptic::wasInactive() {
    if (allDisabled()) {
        return true;
    }
    elapsedInactivity = 0;
    if (inactive) {
        log_i("haptics - toggle indicators - on");
        inactive = false;
        digitalWrite(indicators[selected], HIGH);
        return true;
    }
    return false;
}

void Haptic::toggle_modes() {
    if (wasInactive()) return;

    log_i("haptics - modes");
    controllers[selected]->toggle_modes();
}

void Haptic::cycle_options() {
    static elapsedMillis elapsed;

    if (elapsed < 50 || wasInactive()) return;
    log_i("haptics - speed");
    controllers[selected]->cycle_speed(false);
    controllers[selected]->cycle_color(false);
    elapsed = 0;
}

void Haptic::save_options() {
    if (wasInactive()) return;

    log_i("haptics - save speed");
    controllers[selected]->cycle_speed(true);
    controllers[selected]->cycle_color(true);
}

void Haptic::cycle_brightness() {
    static elapsedMillis elapsed;

    if (elapsed < 50 || wasInactive()) return;
    log_i("haptics - brightness");
    controllers[selected]->cycle_brightness(false);
    elapsed = 0;
}

void Haptic::save_brightness() {
    if (wasInactive()) return;

    log_i("haptics - save brightness");
    controllers[selected]->cycle_brightness(true);
}

void Haptic::toggle_state() {
    if (wasInactive()) return;

    log_i("haptics - state");
    controllers[selected]->toggle_state();
}

void Haptic::cycle_chains() {
    if (wasInactive()) return;

    do {
        selected = (selected + 1) % controllers.size();
    } while (controllers[selected]->disabled());
    log_i("haptics - strand %d", selected);

    for (size_t i = 0; i < indicators.size(); i++) {
        digitalWrite(indicators[i], i == selected ? HIGH : LOW);
    }
}

void Haptic::serve(BleControllerPixels *c) {
    controllers.push_back(c);
}
